import csv
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
import pickle

df = pd.DataFrame(columns=['feat1','feat2'])
idx = range(1,20)
pca = pickle.load(open('Data/pca.sav', 'rb'))

with open('Data/x_test.csv') as csvfile:
    readCSV = csv.reader(csvfile,delimiter=',')
    for i,row in enumerate(readCSV):
        if(i != 0):
            vect = [row[k] for k in idx]
            vect = np.asarray(vect).astype(np.float64)
            vect_comp = pca.transform(vect.reshape(1, -1))
            print(vect_comp)
            dat = {'feat1':vect_comp[0][0], 'feat2':vect_comp[0][1]}
            print(dat)
            dat = pd.DataFrame(data=dat,index=['index'])
            df = df.append(dat,ignore_index=True)
print(df)
df.to_csv('Data/test_cmp.csv')


