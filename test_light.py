import torch.nn as nn
from models import *
from torchvision import transforms
from torch.utils.data import DataLoader
import numpy as np

batch_size = 1
num_threads = 4
model = LSTMLight(37,8,256,4,0.7)
use_gpu = torch.cuda.is_available()
if use_gpu:
    device = torch.device('cuda')
else:
    device = torch.device('cpu')
model = model.to(device)
model.load_state_dict(torch.load("Data/model_final.pt"))
pytorch_total_params = sum(p.numel() for p in model.parameters())
print(pytorch_total_params)
print("Loading challenge dataset")
#challengeDataSet = OzeDataset('./Data/x_train.csv','./Data/y_train.csv','labels.json')

m = torch.load('Data/mout.pt')
M = torch.load('Data/Mout.pt')
torch.save(m,'Data/mout.pt')
torch.save(M,'Data/Mout.pt')
#m = m.to(device)
#M = M.to(device)
print("Done")
print("Loading test dataset")
dataset_eval = OzeDataset('Data/x_test.csv')

m_test = len(dataset_eval)
K = 672

predictions = np.empty((0, 8))
with torch.no_grad():
    for idx, line in enumerate(dataset_eval):
        # Run prediction
        line = torch.Tensor(line[np.newaxis, :, :])
        line = line.to(device)
        netout = model(line).detach().cpu().numpy()
        # De-normalize output
        output =  netout * (M - m + np.finfo(float).eps) + m
        output = output.reshape((672,8))
        print(output.shape)
        predictions = np.append(predictions,output,axis=0)
        print(predictions.shape)
np.savetxt('Data/y_correct_mid.csv',predictions,delimiter=',')
"""
lines_output = predictions.reshape((m_test, -1))
csv_header = [f"{var_name}_{k}" for var_name in dataset_eval.lables_json['X'] for k in range(K)]

final_df = pd.DataFrame(lines_output, columns=csv_header)
print(final_df.head())

indx = np.arange(7500,7500+500)
final_df.insert(0,'index',indx)
final_df.to_csv('Data/y_final.csv',index=False)
print(final_df.head())
"""
