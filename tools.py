import numpy as np
import pandas as pd
import torch
import os

def train_light(model,loader,f_loss,optimizer,schedular,device):
    model.train()
    err = 0
    for i, (inp_dat,out_dat) in enumerate(loader):
        inp_dat = inp_dat.to(device)
        out_dat = out_dat.to(device)
        optimizer.zero_grad()
        est_dat = model(inp_dat)
        loss = f_loss(est_dat,out_dat)

        loss.backward()
        optimizer.step()
        schedular.step()
        err += loss
    return err/len(loader.dataset)

def valid_light(model,loader,f_loss,device):
    with torch.no_grad():
        model.eval()
        err = 0
        
        for i ,(inp_dat,out_dat) in enumerate(loader):
            inp_dat = inp_dat.to(device)
            out_dat = out_dat.to(device)
            est_dat = model(inp_dat)
            loss = f_loss(est_dat,out_dat)

            err += loss
        return err/len(loader.dataset)

            
def generate_unique_logpath(logdir, raw_run_name):
    i = 0
    while(True):
        run_name = raw_run_name + "_" + str(i)
        log_path = os.path.join(logdir, run_name)
        if not os.path.isdir(log_path):
            return log_path
        i = i + 1

def test_light(model,loader):
    pass
