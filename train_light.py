from models import *
from torch.utils.data import DataLoader,dataset
import torchvision.transforms as transforms
import torch
import torch.nn as nn
import os
import sys
import argparse
import os
import sys
from torch.utils.tensorboard import SummaryWriter
from tools import train_light, valid_light,generate_unique_logpath

challengeDataSet = OzeDataset('./Data/x_train.csv','./Data/y_train.csv','labels.json')

num_threads = 4
batch_size = 4


parser = argparse.ArgumentParser()
parser.add_argument("--epochs",help="number of epochs for training",type=int)
args = parser.parse_args()

valid_ratio = 0.1

nb_train = (1-valid_ratio)*len(challengeDataSet)
nb_train = int(nb_train)
nb_valid = len(challengeDataSet) - nb_train
nb_valid = int(nb_valid)


train_dataset, valid_dataset = dataset.random_split(challengeDataSet,[nb_train,nb_valid])
#train_dataset = challengeDataSet
train_loader = DataLoader(dataset=train_dataset,batch_size=batch_size,shuffle=True,num_workers=num_threads)
valid_loader = DataLoader(dataset=valid_dataset,batch_size=batch_size,shuffle=True,num_workers=num_threads)

use_gpu = torch.cuda.is_available()
if use_gpu:
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

top_logdir = "./logs"


f_loss = nn.MSELoss(reduction='mean')
alphas = np.arange(0.1,1,0.1)
for i_a,alpha in enumerate(alphas):
    model = LSTMLight(37,8,256,4,0.7)
    logdir = generate_unique_logpath(top_logdir, "LSTM_final")
    tensorboard_writer   = SummaryWriter(log_dir = logdir)
    optimizer = torch.optim.Adam(model.parameters())
    schedular = torch.optim.lr_scheduler.StepLR(optimizer,step_size=10,gamma=0.5)
    last_loss = -1

    print(len(train_dataset))
    #print(len(valid_dataset))
    for t in range(int(args.epochs)):
        print("Epoch {}".format(t))
        print("##Training : ##")
        train_loss = train_light(model, train_loader, f_loss, optimizer, schedular, device)
        print("Loss : {}".format(train_loss))
        print("######")
        print("\n")
        print("##Validation##")
        valid_loss = valid_light(model,valid_loader,f_loss,device)
        print("Loss : {}".format(valid_loss))
        print("######")
        print("\n")
        if(last_loss == -1):
            last_loss = valid_loss
            torch.save(model.state_dict(),"Data/model_final_{}.pt".format(i_a))
        if(valid_loss < last_loss):
            last_loss = valid_loss
            torch.save(model.state_dict(),"Data/model_final_{}.pt".format(i_a))
        tensorboard_writer.add_scalar('metrics_finals/valid_loss', valid_loss, t)
