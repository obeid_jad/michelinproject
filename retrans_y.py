import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

res_data = np.genfromtxt('Data/y_correct_mid.csv', delimiter=',')
print(res_data.shape)
#idx = [7,0,1,5,6,3,4,2]
#res_data = res_data[:,idx]
#np.savetxt('Data/res_dat_sub.csv',res_data,delimiter=',')
res_trans = np.empty((500,8*672))

import csv

with open('Data/y_correct_mid.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for i,row in enumerate(readCSV):
        l = int(i/672)
        r = i%672
        for j in range(8):
            res_trans[l][j*672 + r] = row[j]
print(res_trans.shape)

cols = []
print(res_trans)



for i in range(672):
  cols.append('Q_AC_OFFICE_{}'.format(i))

for i in range(672):
  cols.append('Q_HEAT_OFFICE_{}'.format(i))

for i in range(672):
  cols.append('Q_PEOPLE_{}'.format(i))

for i in range(672):
  cols.append('Q_EQP_{}'.format(i))

for i in range(672):
  cols.append('Q_LIGHT_H_{}'.format(i))

for i in range(672):
  cols.append('Q_AHU_C_{}'.format(i))

for i in range(672):
  cols.append('Q_AHU_H_{}'.format(i))

for i in range(672):
  cols.append('T_INT_OFFICE_{}'.format(i))


idx = np.arange(7500,500+7500,1)
df = pd.DataFrame(data=res_trans, columns=cols)
df.insert(0,"index",idx,True)

print(df.columns)

print(df.head())

df.to_csv('Data/y_correct_final',index=False)