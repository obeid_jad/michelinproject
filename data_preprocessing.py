import numpy as np
import pandas as pd

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import pickle

scaler = StandardScaler()
pca = PCA(n_components=2)

data_path = 'Data/x_train.csv'
print("Reading Data")

df = pd.read_csv(data_path)
print(df.columns)
df = df[list(df.columns[1:20])]
np_array = df.to_numpy()
scaler.fit_transform(np_array)
np_array = pca.fit_transform(np_array)

kmeans = KMeans(n_clusters=36, random_state=0).fit(np_array)

print(kmeans.predict(np_array[0].reshape(1,-1)))
print(np_array.shape)

pickle.dump(kmeans, open('Data/kmeans.sav', 'wb'))
pickle.dump(pca, open('Data/pca.sav', 'wb'))
plt.scatter(np_array[:,0],np_array[:,1],c=kmeans.labels_)
plt.show()
