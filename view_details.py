import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

data_path = 'Data/x_test.csv'

print("Reading Data")
df = pd.read_csv(data_path)
"""
for i in range(1,18):
    print(list(df.columns[i:i+1]))
    df0 = df[list(df.columns[i:i+1])]
    np_array = df0.to_numpy()
    plt.figure()
    plt.title(str(list(df.columns[i:i+1])))
    plt.plot(np_array,'o')
plt.show()

"""

arr1 = df['airchange_infiltration_vol_per_h'].to_numpy()
arr2 = df['power_VCV_kW_heat'].to_numpy()
arr3 = df['power_VCV_kW_clim'].to_numpy()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(arr1,arr2,arr3)
plt.show()
