from models import *
from torch.utils.data import DataLoader,dataset
import torchvision.transforms as transforms
import torch
import torch.nn as nn
import os
import sys
import argparse
import os
import sys
from torch.utils.tensorboard import SummaryWriter

challengeDataSet = myDataset('./Data/x_train_trans.csv','./Data/train_cmp.csv','./Data/y_train_trans.csv')

num_threads = 4
batch_size = 672*10


parser = argparse.ArgumentParser()
parser.add_argument("--epochs",help="number of epochs for training",type=int)
args = parser.parse_args()

train_dataset = DatasetTransformer(challengeDataSet,transforms.ToTensor())
train_loader = DataLoader(dataset=train_dataset,batch_size=batch_size,shuffle=False,num_workers=num_threads)



model = DeepLSTM(input_dim=20,output_dim=8,n_hidden=50,lstm_layers=2,batch_size=batch_size,dropout=0.6)
f_loss = torch.nn.MSELoss(reduction='mean')
optimizer = torch.optim.Adam(model.parameters())
use_gpu = torch.cuda.is_available()
if use_gpu:
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

top_logdir = "./logs"



def generate_unique_logpath(logdir, raw_run_name):
    i = 0
    while(True):
        run_name = raw_run_name + "_" + str(i)
        log_path = os.path.join(logdir, run_name)
        if not os.path.isdir(log_path):
            return log_path
        i = i + 1

logdir = generate_unique_logpath(top_logdir, "LSTM_based")
tensorboard_writer   = SummaryWriter(log_dir = logdir)

def train(model, loader, f_loss, optimizer,device,writer):
    model.train()
    err = 0
    err_val = 0
    print(type(enumerate(loader)))
    hidden1 = model.initHidden(device)
    hidden2 = model.initHidden(device)
    hidden3 = model.initHidden(device)
    min_loss = -1
    for i, (targets,inputs) in enumerate(loader):
        inputs, targets = inputs.to(device), targets.to(device)
        inputs = inputs.float()
        targets = targets.float()
        if(i > 0 and i%10 == 0):
            with torch.no_grad():
                print("Validation step {}: ".format(i))
                outputs = model(inputs,hidden1,hidden2,hidden3)
                loss = f_loss(outputs[0], torch.squeeze(targets))
                writer.add_scalar('step_metrics/valid_loss', loss, int(i/10))
                err_val += loss
                print(loss)
                if(min_loss == -1):
                    min_loss = loss
                    torch.save(model.state_dict(),"./Data/model_lstm.pt")
                    print('new model saved')
                if(min_loss > loss):
                    min_loss = loss
                    torch.save(model.state_dict(),"./Data/model_lstm.pt")
                    print('new model saved')
        else:
            outputs = model(inputs,hidden1,hidden2,hidden3)
            #targets = targets.squeeze(1)
            loss = f_loss(outputs[0],torch.squeeze(targets))
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            err = err + loss
            #print(loss)
            #print(i)
            #print("\n\n\n")
    print("Trainning Loss : {}".format(err))
    print("Validation Loss : {}".format(err_val))
    return err_val

print("Loading model")
model = model.to(device)
model.load_state_dict(torch.load("Data/model_lstm.pt"))
last_loss = -1
for t in range(args.epochs):
    steps_logdir = generate_unique_logpath(top_logdir, "LSTM_steps")
    writer   = SummaryWriter(log_dir = steps_logdir)
    print("Epoch {}".format(t))
    valid_loss = train(model, train_loader, f_loss, optimizer, device, writer)
    if(last_loss == -1):
        last_loss = valid_loss
    if(valid_loss < last_loss):
        last_loss = valid_loss
        torch.save(model.state_dict(),"Data/model_lstm.pt")
    tensorboard_writer.add_scalar('metrics/valid_loss', valid_loss, t)


