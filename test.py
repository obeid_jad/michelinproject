import torch.nn as nn
from models import *
from torchvision import transforms
from torch.utils.data import DataLoader
import numpy as np

batch_size = 672
num_threads = 4
path = '/content/drive/My Drive/challengeENS'
model = DeepLSTM(input_dim=20,output_dim=8,n_hidden=50,lstm_layers=2,batch_size=batch_size)
use_gpu = torch.cuda.is_available()
if use_gpu:
    device = torch.device('cuda')
else:
    device = torch.device('cpu')
model = model.to(device)
model.load_state_dict(torch.load('Data/model_lstm.pt'))
pytorch_total_params = sum(p.numel() for p in model.parameters())
print(pytorch_total_params)

testdat = testDataset('Data/x_test_trans.csv','Data/test_cmp.csv',transforms.ToTensor())
test_loader = DataLoader(dataset=testdat,batch_size=batch_size,shuffle=False,num_workers=num_threads)

def test(model,loader,device):
  model.eval()
  res_dat = np.empty((0,8))
  for i,dat in enumerate(loader):
    print(i)
    dat = dat.to(device).float()
    hidden1 = model.initHidden(device)
    hidden2 = model.initHidden(device)
    hidden3 = model.initHidden(device)
    output = model(dat,hidden1,hidden2,hidden3)
    res_dat = np.append(res_dat,output[0].detach().cpu().numpy(),axis=0)
    print(output[0].detach().cpu().numpy().shape)
  return res_dat

res = test(model,test_loader,device)
np.savetxt("Data/res_dat.csv", res, delimiter=",")