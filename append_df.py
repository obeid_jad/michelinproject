import pandas as pd

print("Reading files")

df1 = pd.read_csv('Data/x_train_trans_1.csv')
print("1 read")
df2 = pd.read_csv('Data/x_train_trans_2.csv')
print("2 read")
df3 = pd.read_csv('Data/x_train_trans_3.csv')
print("3 read")
df4 = pd.read_csv('Data/x_train_trans_4.csv')
print("4 read")
df5 = pd.read_csv('Data/x_train_trans_5.csv')
print("5 read")
df6 = pd.read_csv('Data/x_train_trans_6.csv')
print("6 read")
df7 = pd.read_csv('Data/x_train_trans_7.csv')
print("7 read")
df8 = pd.read_csv('Data/x_train_trans_8.csv')
print("8 read")


print("Appending files")

df1 = df1.append(df2, ignore_index=True)
df1 = df1.append(df3, ignore_index=True)
df1 = df1.append(df4, ignore_index=True)
df1 = df1.append(df5, ignore_index=True)
df1 = df1.append(df6, ignore_index=True)
df1 = df1.append(df7, ignore_index=True)
df1 = df1.append(df8, ignore_index=True)

print(df1)
df1.to_csv('Data/x_train_trans.csv')

print("Done")