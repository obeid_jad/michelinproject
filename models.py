import torch
import torchvision
import pandas as pd
import numpy as np
from torch.utils.data import Dataset
from torchvision import transforms, utils
import torch.nn as nn
import torch.nn.functional as F
import json

class OzeDataset(Dataset):
    def __init__(self,input_csv,output_csv=None,lables_json='labels.json'):
        with open(lables_json,"r") as json_stream:
            self.lables_json = json.load(json_stream)
        self._x_load(input_csv)
        self._output = None
        if(output_csv):
            self._y_load(output_csv)
    def _x_load(self,input_csv):
        input_dat = pd.read_csv(input_csv)
        m = input_dat.shape[0]
        K = 672

        R = input_dat[self.lables_json["R"]].values
        R = np.tile(R[:,np.newaxis,:],(1,K,1))

        Z = input_dat[[f"{var_name}_{i}" for var_name in self.lables_json["Z"] for i in range(K)]]
        Z = Z.values.reshape((m,-1,K))
        Z = Z.transpose((0,2,1))

        self._input = np.concatenate([Z,R],axis = -1)
        self.M = np.max(self._input,axis=(0,1))
        self.m = np.min(self._input,axis=(0,1))
        
        self._input = (self._input - self.m)/(self.M - self.m + np.finfo(float).eps)
        self._input = self._input.astype(np.float32)
    
    def _y_load(self,output_csv):
        output_dat = pd.read_csv(output_csv)
        m = output_dat.shape[0]
        K = 672

        X = output_dat[[f"{var_name}_{i}" for var_name in self.lables_json["X"] for i in range(K)]]

        X = X.values.reshape(m,-1,K)
        X = X.transpose((0,2,1))

        self._output = X
        self.Mout = np.max(self._output,axis=(0,1))
        self.mout = np.min(self._output,axis=(0,1))
        
        self._output = (self._output - self.mout)/(self.Mout - self.mout + np.finfo(float).eps)
        self._output = self._output.astype(np.float32)
    
    def __len__(self):
        return self._input.shape[0]
    
    def __getitem__(self,idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        if(self._output is not None):
            return (self._input[idx], self._output[idx])
        else:
            return self._input[idx]







class myDataset(Dataset):
    def __init__(self,input_csv,comp_csv,output_csv=None,transform=None):
        self.input_features = pd.read_csv(input_csv)
        self.output_features = pd.read_csv(output_csv)
        self.comp_features = pd.read_csv(comp_csv)
        self.transform = transform

    def __len__(self):
        return len(self.output_features)

    def __getitem__(self,idx):
        if(torch.is_tensor(idx)):
            idx = idx.tolist()
        out_dat = self.output_features.iloc[idx,2:]
        inp_dat = self.input_features.iloc[idx,2:]
        cmp_dat = self.comp_features.iloc[int(idx/672),1:]
        inp_dat = np.array([inp_dat]).astype('float').reshape(-1,18)
        out_dat = np.array([out_dat]).astype('float').reshape(-1,8)
        cmp_dat = np.array([cmp_dat]).astype('float').reshape(-1,2)
        inp_dat = np.append(inp_dat,cmp_dat).astype('float').reshape(-1,20)
        if(self.transform):
            inp_dat = self.transform(inp_dat)
        return {'output':out_dat,'input':inp_dat}


class testDataset(Dataset):
    def __init__(self,input_csv,comp_csv,transform=None):
        self.input_features = pd.read_csv(input_csv)
        self.comp_features = pd.read_csv(comp_csv)
        self.transform = transform

    def __len__(self):
        return len(self.input_features)

    def __getitem__(self,idx):
        if(torch.is_tensor(idx)):
            idx = idx.tolist()
        inp_dat = self.input_features.iloc[idx,1:]
        cmp_dat = self.comp_features.iloc[int(idx/672),1:]
        inp_dat = np.array([inp_dat]).astype('float').reshape(-1,18)
        cmp_dat = np.array([cmp_dat]).astype('float').reshape(-1,2)
        inp_dat = np.append(inp_dat,cmp_dat).astype('float').reshape(-1,20)
        if(self.transform):
            inp_dat = self.transform(inp_dat)
        return inp_dat



class DatasetTransformer(Dataset):
    def __init__(self, base_dataset, transform):
        self.base_dataset = base_dataset
        self.transform = transform

    def __getitem__(self, index):
        inputs = self.base_dataset[index]['input']
        outputs = self.base_dataset[index]['output']
        return outputs, self.transform(inputs)

    def __len__(self):
        return len(self.base_dataset)

class LSTMLight(nn.Module):
    def __init__(self,input_dim,output_dim,n_hidden,lstm_layers,alpha=0.5):
        super(LSTMLight,self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.n_hidden = n_hidden
        self.lstm_layers = lstm_layers
        self.bn1 = nn.BatchNorm1d(672).cuda()
        self.lstm1 = nn.LSTM(self.input_dim,self.n_hidden,num_layers=self.lstm_layers,batch_first=True).cuda()
        self.alpha = alpha
        #self.linear1 = nn.Linear(self.n_hidden,self.n_hidden).cuda()
        #self.bn2 = nn.BatchNorm1d(672).cuda()
        #self.lstm2 = nn.LSTM(self.n_hidden,self.n_hidden,num_layers=self.lstm_layers,dropout=dropout,batch_first=True).cuda()
        #self.bn2 = nn.BatchNorm1d(672).cuda()
        self.lin1 = nn.Sequential(
            nn.Linear(self.n_hidden,self.n_hidden).cuda(),
            nn.Linear(self.n_hidden,self.n_hidden).cuda(),
            nn.Linear(self.n_hidden,self.output_dim).cuda()
        )
        self.non_lin1 = nn.Sequential(
            nn.Linear(self.n_hidden,self.n_hidden).cuda(),
            nn.ReLU(inplace=True).cuda(),
            nn.Linear(self.n_hidden,self.n_hidden).cuda(),
            nn.ReLU(inplace=True).cuda(),
            nn.Linear(self.n_hidden,self.output_dim).cuda(),
            nn.ReLU(inplace=True).cuda()
        )
        #self.linear1 = nn.Linear(self.n_hidden,self.output_dim).cuda()



    def forward(self,x):
        lstm_out,_ = self.lstm1(x)
        #x = self.bn1(lstm_out)
        x1 = self.lin1(lstm_out)
        x2 = self.non_lin1(lstm_out)
        output = F.relu(self.alpha*x1 + (1 - self.alpha*x2))
        return output

class DeepLSTM(nn.Module):
    def __init__(self,input_dim,output_dim,n_hidden,lstm_layers,dropout=0.5):
        super(DeepLSTM,self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.n_hidden = n_hidden
        self.lstm_layers = lstm_layers
        
        self.lstm1 = nn.LSTM(input_dim,n_hidden,num_layers=self.lstm_layers,batch_first=True).cuda()
        self.lin1 = nn.Linear(n_hidden,n_hidden).cuda()
        self.lstm2 = nn.LSTM(n_hidden,n_hidden,num_layers=self.lstm_layers,batch_first=True).cuda()
        self.lin2 = nn.Linear(n_hidden,n_hidden).cuda()
        self.lstm3 = nn.LSTM(n_hidden,n_hidden,num_layers=self.lstm_layers,batch_first=True).cuda()
        self.lin3 = nn.Linear(n_hidden,output_dim).cuda()

    def forward(self,x):
        #out1 = self.lin1(x)
        lstmout,_ = self.lstm1(x)
        x = F.relu(self.lin1(lstmout))
        
        lstmout,_ = self.lstm2(x)
        x = F.relu(self.lin2(lstmout))
        
        lstmout,_ = self.lstm3(x)
        out = self.lin3(lstmout)
        
        return out
