import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from mpl_toolkits.mplot3d import Axes3D


scaler = StandardScaler()
pca = PCA(n_components=3)
data_path = 'Data/x_test.csv'
print("Reading Data")

df = pd.read_csv(data_path)
df = df[list(df.columns[1:17])]
np_array = df.to_numpy()
scaler.fit_transform(np_array)
np_array = pca.fit_transform(np_array)

print(np_array.shape)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(np_array[:,0],np_array[:,1],np_array[:,2])
plt.show()
